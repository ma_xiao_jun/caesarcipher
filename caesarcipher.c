/******************
File name: caesarcipher
Author:马小珺 		ID：2018240381013		Version：3.0版		Date：2020.3.13
Description: 用命令行进行加密解密操作

*******************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define MAX 1024

int Charnum(char* str)   //统计文本中的字符
{
    int num = 0;
    while(*str ++ != '\0')
    {
        num ++;
    }
    return num;
}

int main(int argc, char* argv[])
{
    int encrypt();
    int decrypt();
    if(argc != 4)    //判断输入的参数是否是4个
    {
        printf("\nInput parameter error.\n");
        return 0;
    }
    if((**(argv + 1)) == '-')    //如果第二个参数开头是“-”，则进行下面的选择
    {
        switch(*(argv[1] + 1))
        {
        case 'e':    //调用加密函数
            encrypt(argv[2],argv[3]);
            break;
        case 'd':    //调用解密函数
            decrypt(argv[2],argv[3]);
            break;
        default:    //其余的输入都是错误参数
            printf("\nInput parameter error.\n");
            break;
        }
    }
    return 0;
}

int encrypt(char *ch1,char *ch2)    //加密函数
{
    char str[MAX];
    char c[MAX]= "\0";
    char m[MAX]= "\0";
    FILE *en,*de;
    int i,j = 0,count;
    if((en = fopen(ch1,"r+")) == NULL)
    {
        printf("\nCan't open file.\n");
        return 0;
    }
    while(fgets(str,MAX,en)!= NULL)    //逐行读取文件内容
    {
        count = Charnum(str);
        for(i = 0;str[i] != '\0';i ++)    //将明文文本中的英文字符储存在数组m中
        {
            if((str[i] >= 'a' && str[i] <= 'z') ||(str[i] >= 'A' && str[i] <= 'Z'))
            {
                m[j] = str[i];
                j ++;
            }
        }
    }
    fclose(en);
    printf("\nplaintext:%s\n\n",m);
    if((de = fopen(ch2,"w+")) == NULL)
    {
        de = fopen(ch2,"w+");    //如果文件不存在就创建一个
    }
    for(i = 0;m[i] != '\0';i ++)     //按照字母的大小写分别进行加密，即密文的大小写同明文一样
    {
        if(m[i] >= 'A' && m[i] <= 'Z')
        {
            c[i] = ('A'+(m[i]-'A'+3)%26);
            fprintf(de,"%c",c[i]);    //写入密文1e.txt中
        }
        if(m[i] >= 'a' && m[i] <= 'z')
        {
            c[i] = ('a'+(m[i]-'a'+3)%26);
            fprintf(de,"%c",c[i]);
        }
    }
    printf("ciphertext:%s\n",c);
    fclose(de);
}

int decrypt(char *ch1,char *ch2)    //解密函数
{
    char str[MAX];
    char c[MAX]= "\0";
    char m[MAX]= "\0";
    FILE *en,*de;
    int i,j = 0,count,k;
    if((de = fopen(ch1,"r+")) == NULL)    //逐行读取文件内容
    {
        printf("\nCan't open file.\n");
        return 0;
    }
    while(fgets(str,MAX,de)!= NULL)
    {
        count = Charnum(str);
        for(i = 0;str[i] != '\0';i ++)    //将密文文本中的英文字符储存在数组c中
        {
            if((str[i] >= 'a' && str[i] <= 'z') ||(str[i] >= 'A' && str[i] <= 'Z'))
            {
                c[j] = str[i];
                j ++;
            }
        }
    }
    fclose(de);
    printf("\nciphertext:%s\n\n",c);
    if((en = fopen(ch2,"w+")) == NULL)
    {
        en = fopen(ch2,"w+");    //如果文件不存在就创建一个
    }
    for(i = 0;c[i] != '\0';i ++)    //按照字母的大小写分别进行解密，即明文的大小写同密文一样
    {
        if(c[i] >= 'A' && c[i] <= 'Z')
        {
            k = (c[i]-'A'-3);
            if(k < 0)
                k += 26;    //如果超出范围，就加上26，因为同余
            m[i] = ('A'+k%26);
            fprintf(en,"%c",m[i]);    //写入明文1.txt中
        }
        if(c[i] >= 'a' && c[i] <= 'z')
        {
            k = (c[i]-'a'-3);
            if(k < 0)
                k += 26;
            m[i] = ('a'+k%26);
            fprintf(en,"%c",m[i]);
        }
    }
    printf("plaintext:%s\n",m);
    fclose(en);
}

